
# New Octa-Mold Calculations

## What will be the new relative density
I would like near an order of magnitude increase in relative density. To estimate what a reasonable strut might look like, I expanded the current strut cross-section so that it would intersect with the bottom edge of the node. (See image below)
![relative-density-estimation](LargestStrut-.png). 
This strut makes an approximately 0.02 % relative density lattice. I will use this as a baseline design. 

This has a cross-sectional area of 0.0227 square inches, kite sides are .127" and .179" respectively. Strut length about 1.8". 

## Estimating Load
We will use Ashby's scaling equations to estimate the load that this lattice will take relative to the current lattice. We know that cuboct is between linear and quadratic scaling. 

The worst case scenario is that cuboct is quadratic scaling and that the order of magnitude increase in relative density leads to a 100x increase in load capacity. 

If a single Ultem2200 currently yields at 40N, this would mean a design load of 4,000N. Assuming 1.5 scaling would give a design load of 1280N. 







```python
N_2_lf= 0.224809
lb_load= 1280*N_2_lf
lb_load
```




    287.75552



For a current RTP 2187 4x4x4 assembly had a tensile failure strength of 0.03 MPa. This translates to a failure load of 


```python
RTP_strength = 0.03 #MPa, 4x4x4, original voxels
Area= (4*76.2)*(4*76.2) #3 in = 76.2 mm
Failure_Load= RTP_strength*Area #N (MPa*mm)
Failure_load_per_voxel= Failure_Load/16. 
Failure_load_per_voxel
```




    174.19320000000002



This is the failure load in Newtons per voxel. If we increase by an order of magnitude and assume 1.5 scaling, this gives an expected failure load of 


```python
New_failure_load_per= Failure_load_per_voxel* 31.6227766 #10^1.5 = 31.6227766
New_failure_load_per #N
```




    5508.472648839121




```python
N_2_lf= 0.224809
lb_load= New_failure_load_per*N_2_lf
lb_load
```




    1238.354227712874



** Because we are bolting material that is softer than the bolt, the bolt will take all external load AND the preload**

This places some quite stringent requirements on the bolt, in the case of RTP 2187. 
